NativeDumper
=========

Native module dumper, just select a process
do right mouse click and choose "Dump main module"
or "Modules" to enumerate modules, select target module,
do right mouse click an choose "Dump".

Advantage over other dumpers:
- Small dump file size ( with default dumping options
more exactly with "Fix Raw" option unchecked (off). 

Created on Visual C++ 6.0

